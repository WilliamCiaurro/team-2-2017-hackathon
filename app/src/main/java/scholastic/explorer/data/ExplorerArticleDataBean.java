package scholastic.explorer.data;

import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by IanCou on 10/24/2017.
 */

public class ExplorerArticleDataBean {

    private String title;
    private boolean scholasticContent = false;
    private long articleID;
    private String articleBody;
    private String articleAuthor;
    private String featuredImage;
    private List<String> tags = new ArrayList();
    private LatLng coordinate;


    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public boolean isScholasticContent() {
        return scholasticContent;
    }

    public void setScholasticContent(boolean scholasticContent) {
        this.scholasticContent = scholasticContent;
    }

    public long getArticleID() {
        return articleID;
    }

    public void setArticleID(long articleID) {
        this.articleID = articleID;
    }

    public String getArticleBody() {
        return articleBody;
    }

    public void setArticleBody(String articleBody) {
        this.articleBody = articleBody;
    }

    public String getArticleAuthor() {
        return articleAuthor;
    }

    public void setArticleAuthor(String articleAuthor) {
        this.articleAuthor = articleAuthor;
    }

    public String getFeaturedImage() {
        return featuredImage;
    }

    public void setFeaturedImage(String featuredImage) {
        this.featuredImage = featuredImage;
    }

    public List<String> getTags() {
        return tags;
    }

    public void setTags(List<String> tags) {
        this.tags = tags;
    }

    public LatLng getCoordinate() {
        return coordinate;
    }

    public void setCoordinate(LatLng coordinate) {
        this.coordinate = coordinate;
    }

    ExplorerArticleDataBean(String title, boolean scholasticContent, long articleId, String articleBody, String articleAuthor, String featuredImage, List tags, LatLng coordinate) {
        this.setTitle(title);
        this.setScholasticContent(isScholasticContent());
        this.setCoordinate(coordinate);
        this.setArticleBody(articleBody);
        this.setArticleAuthor(articleAuthor);
        this.setFeaturedImage(featuredImage);
        //this.seta
    }
}
