package scholastic.explorer;

import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import java.util.ArrayList;

import scholastic.explorer.data.ExplorerSavedLandmarkDataBean;

public class Main extends AppCompatActivity {
    private String TAG = "Scholastic Explorer";

    ListView mDrawerList;
    RelativeLayout mDrawerPane;
    private ActionBarDrawerToggle mDrawerToggle;
    private DrawerLayout mDrawerLayout;

    ArrayList<NavItem> mNavItems = new ArrayList<NavItem>();

    ListView locationsList;
    LinearLayout locationsLayout;
    private ScrollView mainScrollView;

    ArrayList<ExplorerSavedLandmarkDataBean> locationItems = new ArrayList<ExplorerSavedLandmarkDataBean>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        // More info: http://codetheory.in/difference-between-setdisplayhomeasupenabled-sethomebuttonenabled-and-setdisplayshowhomeenabled/
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        init();
        initSideDrawer();
        initLocationScrollView();


    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        mDrawerToggle.syncState();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Pass the event to ActionBarDrawerToggle
        // If it returns true, then it has handled
        // the nav drawer indicator touch event
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }

        // Handle your other action bar items...

        return super.onOptionsItemSelected(item);
    }

    private void init(){

        Button goToMapBtn = (Button) findViewById(R.id.seeCurrentView);
        goToMapBtn.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick( View v ){
                Intent i = new Intent(getApplicationContext(), MapsActivity.class);
                startActivity(i);
            }
        });

    }

    private void initLocationScrollView() {
        //locationItems.add(new NavItem("Independance Hall", "123 Main Avenue, Philadelphia PA", R.drawable.ic_home));
       // locationItems.add(new NavItem("Edge Allan Poe House", "123 Main Avenue, Philadelphia PA", R.drawable.ic_settings));
       // locationItems.add(new NavItem("The Liberty Bell", "123 Main Avenue, Philadelphia PA", R.drawable.ic_about_info));
        locationItems=ExplorerSavedLandmarkDataBean.findByCity("Philadelphia","PA");
        // LocationLayout
        mainScrollView = (ScrollView) findViewById(R.id.mainScrollView);



        // Populate the Navigtion Drawer with options
        locationsLayout = (LinearLayout) findViewById(R.id.scrollViewLinear);
        locationsList = (ListView) findViewById(R.id.locationList);
        ScrollViewAdapter adapter = new ScrollViewAdapter(this, locationItems);
        locationsList.setAdapter(adapter);

        locationsList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                selectLocationItemIntent();
            }
        });

    }

    private void initSideDrawer(){

        mNavItems.add(new NavItem("Home", "Meetup destination", R.drawable.ic_home));
        mNavItems.add(new NavItem("Preferences", "Change your preferences", R.drawable.ic_settings));
        mNavItems.add(new NavItem("About", "Get to know about us", R.drawable.ic_about_info));

        // DrawerLayout
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawerLayout);

        // Populate the Navigtion Drawer with options
        mDrawerPane = (RelativeLayout) findViewById(R.id.drawerPane);
        mDrawerList = (ListView) findViewById(R.id.navList);
        DrawerListAdapter adapter = new DrawerListAdapter(this, mNavItems);
        mDrawerList.setAdapter(adapter);

        // Drawer Item click listeners
        mDrawerList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                selectItemFromDrawer(position);
            }
        });


        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, R.string.drawer_open, R.string.drawer_close) {
            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);

                invalidateOptionsMenu();
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
                Log.d(TAG, "onDrawerClosed: " + getTitle());

                invalidateOptionsMenu();
            }

        };

        mDrawerLayout.setDrawerListener(mDrawerToggle);
    }

    /*
    * Called when a particular item from the navigation drawer
    * is selected.
    * */
    private void selectItemFromDrawer(int position) {
        Fragment fragment = new PreferencesFragment();

        FragmentManager fragmentManager = getFragmentManager();
        fragmentManager.beginTransaction()
                .replace(R.id.mainContent, fragment)
                .commit();

        mDrawerList.setItemChecked(position, true);
        setTitle(mNavItems.get(position).mTitle);

        // Close the drawer
        mDrawerLayout.closeDrawer(mDrawerPane);
    }

    private void selectLocationItemIntent(){

        Intent i = new Intent(getApplicationContext(), LocationSummary.class);
        startActivity(i);

    }

    class NavItem {
        String mTitle;
        String mSubtitle;
        int mIcon;

        public NavItem(String title, String subtitle, int icon) {
            mTitle = title;
            mSubtitle = subtitle;
            mIcon = icon;
        }
    }

    class DrawerListAdapter extends BaseAdapter {

        Context mContext;
        ArrayList<NavItem> mNavItems;

        public DrawerListAdapter(Context context, ArrayList<NavItem> navItems) {
            mContext = context;
            mNavItems = navItems;
        }

        @Override
        public int getCount() {
            return mNavItems.size();
        }

        @Override
        public Object getItem(int position) {
            return mNavItems.get(position);
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }


        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View view;

            if (convertView == null)
            {
                LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                view = inflater.inflate(R.layout.draweritem, null);
            }
            else
            {
                view = convertView;
            }

            TextView titleView = (TextView) view.findViewById(R.id.title);
            TextView subtitleView = (TextView) view.findViewById(R.id.subTitle);
            ImageView iconView = (ImageView) view.findViewById(R.id.icon);

            titleView.setText( mNavItems.get(position).mTitle );
            subtitleView.setText( mNavItems.get(position).mSubtitle );
            iconView.setImageResource(mNavItems.get(position).mIcon );
            setTitle(mNavItems.get(position).mTitle);

            return view;

        }

    }


    class ScrollViewAdapter extends BaseAdapter {

        Context mContext;
        ArrayList<ExplorerSavedLandmarkDataBean> locationItems;

        public ScrollViewAdapter(Context context, ArrayList<ExplorerSavedLandmarkDataBean> locationItemsM) {
            mContext = context;
            this.locationItems = locationItemsM;
        }

        @Override
        public int getCount() {
            return locationItems.size();
        }

        @Override
        public Object getItem(int position) {
            return locationItems.get(position);
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }


        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View view;

            if (convertView == null)
            {
                LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                view = inflater.inflate(R.layout.location_item, null);
            }
            else
            {
                view = convertView;
            }

            TextView titleView = (TextView) view.findViewById(R.id.title);
            TextView subtitleView = (TextView) view.findViewById(R.id.subTitle);
            ImageView iconView = (ImageView) view.findViewById(R.id.icon);
            ExplorerSavedLandmarkDataBean bean=(ExplorerSavedLandmarkDataBean) locationItems.get(position);
            titleView.setText( bean.getPlaceName() );
            subtitleView.setText( bean.getStreetAddress() );
            iconView.setImageResource( bean.getIcon());
            setTitle(bean.getPlaceName());

            return view;

        }

    }

}
