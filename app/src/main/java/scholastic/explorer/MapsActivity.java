package scholastic.explorer;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import scholastic.explorer.data.ExplorerSavedLandmarkDataBean;

import static com.google.android.gms.location.LocationServices.FusedLocationApi;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        LocationListener, GoogleMap.OnMarkerClickListener {

    private Map<String, Marker> markerMap = new HashMap<>();
    private Map<String, ExplorerSavedLandmarkDataBean> explorerSavedLandmarkDataBeanMap  = new HashMap<>();
    private LocationManager locationManager;
    private GoogleMap mMap;
    private GoogleApiClient client;
    private LocationRequest locationRequest;
    private Location lastlocation;
    private Location mCameraPosition;
    private Marker currentLocationmMarker;
    private Geocoder geocoder;


    public static final int REQUEST_LOCATION_CODE = 99;
    int PROXIMITY_RADIUS = 10000;
    double latitude, longitude;

    private static final String KEY_CAMERA_POSITION = "camera_position";
    private static final String KEY_LOCATION = "location";

    @Override
    public boolean onMarkerClick(final Marker marker) {
        Marker myMarker = markerMap.get(marker.getId());
        if (myMarker != null) {

                    ExplorerSavedLandmarkDataBean bean = explorerSavedLandmarkDataBeanMap.get(marker.getId());
                    ImageView scholasticPin = (ImageView) findViewById(R.id.scholastic_pin);
                    scholasticPin.setImageResource(bean.getIcon());
                    scholasticPin.setVisibility(View.VISIBLE);

                    TextView title = (TextView) findViewById(R.id.sch_loc);
            title.setText(bean.getPlaceName());
            title.setTextSize(20f);
            title.setVisibility(View.VISIBLE);

                    TextView address = (TextView) findViewById(R.id.sch_loc_addr);
            address.setText(bean.getStreetAddress());
            address.setTextSize(20f);
            address.setVisibility(View.VISIBLE);

                TextView url =  (TextView) findViewById(R.id.classroom_magazine);
                url.setText("View Classroom Materials");
                url.setVisibility(View.VISIBLE);

            return true;
                }

        return false;
    }

    public void goToContent(View view){
        Intent intent = new Intent(this, scholastic.explorer.LocationSummary.class);

    }
    @Override
    protected void onSaveInstanceState(Bundle outState) {
        if (mMap != null) {
            outState.putParcelable(KEY_CAMERA_POSITION, mMap.getCameraPosition());
            outState.putParcelable(KEY_LOCATION, lastlocation);
            super.onSaveInstanceState(outState);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState != null) {
            lastlocation = savedInstanceState.getParcelable(KEY_LOCATION);
            mCameraPosition = savedInstanceState.getParcelable(KEY_CAMERA_POSITION);
        }


        setContentView(R.layout.mapsactivity);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            checkLocationPermission();

        }
        locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.aamirsMap);
        mapFragment.getMapAsync(this);
        geocoder = new Geocoder(this);
    }

    public void addToMap() {

        if (mMap != null) {
            ArrayList<ExplorerSavedLandmarkDataBean> list = ExplorerSavedLandmarkDataBean.findByCity("Philadelphia", "PA");
            for(ExplorerSavedLandmarkDataBean bean : list){
                Log.i("We're getting philly" , "yep");
                LatLng location = bean.getCoordinate();

                Marker marker = mMap.addMarker(new MarkerOptions()
                        .position(location)
                        .title(bean.getPlaceName())
                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_map_pin)));
                markerMap.put(marker.getId(), marker);
                explorerSavedLandmarkDataBeanMap.put(marker.getId(), bean);
                mMap.moveCamera(CameraUpdateFactory.newLatLng(location));


            }

        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_LOCATION_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                        if (client == null) {
                            bulidGoogleApiClient();
                        }
                        mMap.setMyLocationEnabled(true);
                    }
                } else {
                    Toast.makeText(this, "Permission Denied", Toast.LENGTH_LONG).show();
                }
        }
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            bulidGoogleApiClient();
            mMap.setMyLocationEnabled(true);
            addToMap();
            mMap.setOnMarkerClickListener(this);
        }
    }


    protected synchronized void bulidGoogleApiClient() {
        client = new GoogleApiClient.Builder(this).addConnectionCallbacks(this).addOnConnectionFailedListener(this).addApi(LocationServices.API).build();
        client.connect();

    }

    @Override
    public void onLocationChanged(Location location) {

        latitude = location.getLatitude();
        longitude = location.getLongitude();
        lastlocation = location;
        if (currentLocationmMarker != null) {
            currentLocationmMarker.remove();

        }
        Log.d("lat = ", "" + latitude);
        LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());

        MarkerOptions markerOptions = new MarkerOptions();
        markerOptions.position(latLng);
        markerOptions.title("Current Location");
        markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE));

        currentLocationmMarker = mMap.addMarker(markerOptions);
        markerMap.put(currentLocationmMarker.getId(), currentLocationmMarker);
        mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
        mMap.animateCamera(CameraUpdateFactory.zoomBy(10));
        if (client != null) {
            FusedLocationApi.removeLocationUpdates(client, this);
        }
    }


    /*public void onClick(View v)
    {

       if(v.getId() == R.id.B_search) {
           EditText tf_location = findViewById(R.id.TF_location);
           String location = tf_location.getText().toString();
           List<Address> addressList;


           if (!location.equals("")) {
               Geocoder geocoder = new Geocoder(this);

               try {
                   addressList = geocoder.getFromLocationName(location, 5);

                   if (addressList != null) {
                       for (int i = 0; i < addressList.size(); i++) {
                           LatLng latLng = new LatLng(addressList.get(i).getLatitude(), addressList.get(i).getLongitude());
                           MarkerOptions markerOptions = new MarkerOptions();
                           markerOptions.position(latLng);
                           markerOptions.title(location);
                           mMap.addMarker(markerOptions);
                           mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
                           mMap.animateCamera(CameraUpdateFactory.zoomTo(10));
                       }
                   }
               } catch (IOException e) {
                   e.printStackTrace();
               }
           }
       }
       else{
           mMap.clear();
           String hospital = "hospital";
           String url = getUrl(latitude, longitude, hospital);
           dataTransfer[0] = mMap;
           dataTransfer[1] = url;

           getNearbyPlacesData.execute(dataTransfer);
           Toast.makeText(MapsActivity.this, "Showing Nearby Hospitals", Toast.LENGTH_SHORT).show();
       }
    }*/


    private String getUrl(double latitude, double longitude) {

        StringBuilder googlePlaceUrl = new StringBuilder("https://maps.googleapis.com/maps/api/place/nearbysearch/json?");
        googlePlaceUrl.append("location=" + latitude + "," + longitude);
        googlePlaceUrl.append("&radius=" + PROXIMITY_RADIUS);
        googlePlaceUrl.append("&sensor=true");
        googlePlaceUrl.append("&key=" + "AIzaSyBLEPBRfw7sMb73Mr88L91Jqh3tuE4mKsE");

        Log.d("MapsActivity", "url = " + googlePlaceUrl.toString());

        return googlePlaceUrl.toString();
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

        locationRequest = new LocationRequest();
        locationRequest.setInterval(100);
        locationRequest.setFastestInterval(1000);
        locationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);


        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            LocationServices.FusedLocationApi.requestLocationUpdates(client, locationRequest, this);
            /*locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, this);
            LocationServices.FusedLocationApi.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, this);*/
        }
    }


    public boolean checkLocationPermission() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_FINE_LOCATION)) {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_LOCATION_CODE);
            } else {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_LOCATION_CODE);
            }
            return false;

        } else
            return true;
    }


    @Override
    public void onConnectionSuspended(int i) {
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
    }
}