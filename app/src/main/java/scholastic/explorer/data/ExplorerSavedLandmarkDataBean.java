package scholastic.explorer.data;

/**
 * Created by IanCou on 10/23/2017.
 */

import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;
import java.util.Iterator;

import scholastic.explorer.R;

public class ExplorerSavedLandmarkDataBean {
    public static final ArrayList places = new ArrayList<ExplorerSavedLandmarkDataBean>();
    public static int placeIDCounter = 0;

    static {
        double lat = 0;
        double lon = 0;
        LatLng coordinate = new LatLng(lat, lon);
        String placeName = "";
        String city = "";
        String state = "";
        String streetAddress = "";
        boolean isScholastic = true;
        int icon = R.mipmap.ic_liberty_bell;
        city = "Philadelphia";
        state = "PA";
        placeName = "Liberty Bell";
        lat = 39.9496;
        lon = -75.1503;
        streetAddress = "N 6th St & Market St, Philadelphia, PA 19106";
        coordinate = new LatLng(lat, lon);
        ExplorerSavedLandmarkDataBean bean = new ExplorerSavedLandmarkDataBean(coordinate, placeName, city, state, streetAddress, icon, isScholastic);
        places.add(bean);
        city = "Philadelphia";
        state = "PA";
        placeName = "Independence Hall";

        lat = 39.9489;
        lon = -75.1500;
        icon = R.mipmap.ic_independence_hall;
        streetAddress = "520 Chestnut Street between 5th and 6th streets, Philadelphia, Pennsylvania";
        coordinate = new LatLng(lat, lon);
        bean = new ExplorerSavedLandmarkDataBean(coordinate, placeName, city, state, streetAddress, icon, isScholastic);
        places.add(bean);
        city = "Philadelphia";
        state = "PA";
        placeName = "Philadelphia City Hall";
        lat = 39.9524;
        lon = -75.1636;
        icon = R.mipmap.ic_phil_city_hall;
        streetAddress = "1401 John F Kennedy Blvd, Philadelphia, PA 19102";
        coordinate = new LatLng(lat, lon);
        bean = new ExplorerSavedLandmarkDataBean(coordinate, placeName, city, state, streetAddress, icon, isScholastic);
        places.add(bean);
        city = "Philadelphia";
        state = "PA";
        placeName = "Edgar Allan Poe House";
        lat = 39.9620;
        lon = -75.1499;
        icon = R.mipmap.ic_edgar_poe;
        streetAddress = "532 N 7th St, Philadelphia, PA 19123";
        coordinate = new LatLng(lat, lon);
        bean = new ExplorerSavedLandmarkDataBean(coordinate, placeName, city, state, streetAddress, icon, isScholastic);
        places.add(bean);
        city = "Philadelphia";
        state = "PA";
        placeName = "Franklin Institute";
        lat = 39.9582;
        lon = -75.1731;
        isScholastic=false;
        icon = R.mipmap.ic_franklin_institute;
        streetAddress = "222 N 20th St, Philadelphia, PA 19103";
        coordinate = new LatLng(lat, lon);
        bean = new ExplorerSavedLandmarkDataBean(coordinate, placeName, city, state, streetAddress, icon, isScholastic);
        places.add(bean);


    }

    private long placeId;
    private String placeName;
    private LatLng coordinate;
    private String city;
    private String state;
    private String streetAddress;
    private int icon;
    private boolean scholasticContentExists = false;


    public ExplorerSavedLandmarkDataBean(LatLng coordinate, String placeName, String city, String state, String streetAddress, int icon, boolean isScholastic) {
        this.setCoordinate(coordinate);
        this.setPlaceName(placeName);
        this.setPlaceId(placeIDCounter++);
        this.setCity(city);
        this.setState(state);
        this.setStreetAddress(streetAddress);
        this.setIcon(icon);
        this.setScholasticContentExists(isScholastic);

    }

    public static int getPlaceIDCounter() {
        return placeIDCounter;
    }

    public static void setPlaceIDCounter(int placeIDCounter) {
        ExplorerSavedLandmarkDataBean.placeIDCounter = placeIDCounter;
    }

    public static ArrayList findByCity(String city, String state) {
        ArrayList results = new ArrayList<ExplorerSavedLandmarkDataBean>();
        Iterator e = places.iterator();
        while (e.hasNext()) {
            ExplorerSavedLandmarkDataBean bean = (ExplorerSavedLandmarkDataBean) e.next();
            if (bean.getCity().equalsIgnoreCase(city) && bean.getState().equalsIgnoreCase(state)) {
                results.add(bean);
            }
        }
        return results;
    }

    public long getPlaceId() {
        return placeId;
    }

    public void setPlaceId(long placeId) {
        this.placeId = placeId;
    }

    public LatLng getCoordinate() {
        return coordinate;
    }

    public void setCoordinate(LatLng coordinate) {
        this.coordinate = coordinate;
    }

    public String getPlaceName() {
        return placeName;
    }

    public void setPlaceName(String placeName) {
        this.placeName = placeName;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getStreetAddress() {
        return streetAddress;
    }

    public void setStreetAddress(String streetAddress) {
        this.streetAddress = streetAddress;
    }

    public int getIcon() {
        return icon;
    }

    public void setIcon(int icon) {
        this.icon = icon;
    }

    public boolean isScholasticContentExists() {
        return scholasticContentExists;
    }

    public void setScholasticContentExists(boolean scholasticContentExists) {
        this.scholasticContentExists = scholasticContentExists;
    }
}
